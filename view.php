<?php echo validation_errors(); ?>
<?php echo form_open('member/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="password" class="col-md-4 control-label">Password</label>
		<div class="col-md-8">
			<input type="password" name="password" value="<?php echo $this->input->post('password'); ?>" class="form-control" id="password" />
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-md-4 control-label">Email</label>
		<div class="col-md-8">
			<input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" />
		</div>
	</div>
	<div class="form-group">
		<label for="fullname" class="col-md-4 control-label">Fullname</label>
		<div class="col-md-8">
			<input type="text" name="fullname" value="<?php echo $this->input->post('fullname'); ?>" class="form-control" id="fullname" />
		</div>
	</div>
	<div class="form-group">
		<label for="phone" class="col-md-4 control-label">Phone</label>
		<div class="col-md-8">
			<input type="text" name="phone" value="<?php echo $this->input->post('phone'); ?>" class="form-control" id="phone" />
		</div>
	</div>
	<div class="form-group">
		<label for="address" class="col-md-4 control-label">Address</label>
		<div class="col-md-8">
			<input type="text" name="address" value="<?php echo $this->input->post('address'); ?>" class="form-control" id="address" />
		</div>
	</div>
	<div class="form-group">
		<label for="dob" class="col-md-4 control-label">Dob</label>
		<div class="col-md-8">
			<input type="text" name="dob" value="<?php echo $this->input->post('dob'); ?>" class="form-control" id="dob" />
		</div>
	</div>
	<div class="form-group">
		<label for="joindate" class="col-md-4 control-label">Joindate</label>
		<div class="col-md-8">
			<input type="text" name="joindate" value="<?php echo $this->input->post('joindate'); ?>" class="form-control" id="joindate" />
		</div>
	</div>
	<div class="form-group">
		<label for="level" class="col-md-4 control-label">Level</label>
		<div class="col-md-8">
			<input type="text" name="level" value="<?php echo $this->input->post('level'); ?>" class="form-control" id="level" />
		</div>
	</div>
	<div class="form-group">
		<label for="flag" class="col-md-4 control-label">Flag</label>
		<div class="col-md-8">
			<input type="text" name="flag" value="<?php echo $this->input->post('flag'); ?>" class="form-control" id="flag" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>
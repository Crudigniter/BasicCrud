<?php

////Controller

	public function companysetting(){
		if($this->input->post()){
			$id = $this->input->post("id");
			
			$params = array(
					'companyname'=>$this->input->post("companyname"),
					'companyaddress'=>$this->input->post("companyaddress")
			);
				
			$action = $this->Model_admin->updatecompany($id,$params);
			
			if($action){
					$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">
												<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
												<strong>Compnay Data !</strong> Updated  
												</div>');
			}
	
			 redirect('Admin/companysetting');
		}else{
			$data['companydata']	= $this->Model_admin->companydata();
			$this->load->view('admin/companysetting_view', $data);
		}
 
	}
	
	
///Model_admin-
   function updatecompany($id,$params){
        $this->db->where('id',$id);
        $response = $this->db->update('companydata',$params);
        if($response)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }	


?>
//View

								<?php if($companydata){?>
								<?php foreach($companydata as $r){?>
                                    <form role="form" action="<?php echo base_url();?>admin/companysetting" method="POST">
									<input type="hidden" name="id" value="<?php echo  $r->id;?>">
								    <div class="row">	
										 <div class="col-md-6">
											<div class="form-group">
												<label>Company Name</label>
												<input type="text" name="companyname" class="form-control" value="<?php echo  $r->companyname;?>" required/>
											</div>
										</div>
										
										<div class="col-md-6">									
											 <div class="form-group">
												<label>Company  Address </label>
												<input type="text" name="companyaddress"  value="<?php echo  $r->companyaddress;?>"  class="form-control" required/>
											</div>
										</div>
									</div>
								<br/>
									  <div class="row">	
									  	<div class="col-md-6">
											
										</div>
										<div class="col-md-6">
											 <div class="form-group">
												 <input type="submit"  class="btn btn-primary pull-right" value="Update"/>
											</div>
										</div>
									  </div>
								<?php 
									}
								}?>	
																				
                                    </form> 
	
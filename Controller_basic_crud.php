<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Member_model');
    } 

	

//Home Page
	public function index(){
		$data['title'] = "Home";
		$data['content'] = $this->load->view('home/home_view', $data, true);
		$this->load->view('home/main_view', $data);
	}
	
//Signup Page	
	public function signup(){
		if($this->input->post()){
			
			$this->load->library('form_validation');

			$this->form_validation->set_rules('email','Email','required|is_unique[member.email]');
			$this->form_validation->set_rules('password','Password','required');
			$this->form_validation->set_rules('fullname','Fullname','required');
			$this->form_validation->set_rules('phone','Phone','required');
			
			if($this->form_validation->run())     
			{   
				$params = array(
					'password' => $this->input->post('password'),
					'email' => $this->input->post('email'),
					'fullname' => $this->input->post('fullname'),
					'phone' => $this->input->post('phone'),
					'address' => $this->input->post('address'),
					'dob' => $this->input->post('dob'),
					'joindate' => time(),
					'level' =>0,
					'flag' => 1,
				);
				
				$member_id = $this->Member_model->add_member($params);
				
				$this->session->set_flashdata('msg','<div class="alert alert-warning alert-dismissible">
														<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
														<h4><i class="icon fa fa-check"></i> Notice !!!</h4>
														 Location already Added
													  </div>');
				//redirect('home/congratulation');
				redirect('home/signup');
			}

			
		}else{
			$data['title'] = "Signup";
			$data['content'] = $this->load->view('home/signup_view', $data, true);
			$this->load->view('home/main_view', $data);
		}
		
		
	}
	
	
//Login Page
	//Login Page
	public function login(){
		if($this->input->post()){
			$this->Member_model->validate();
			$url = base_url()."member";
			if($this->session->userdata('member_login')==true){
		    	redirect($url, 'refresh');
			}else{
				$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                Wrong Username or Password
              </div>');
			  	redirect('home');
			}
		}else{
			$data['title'] = "Login";
			$data['content'] = $this->load->view('home/login_view', $data, true);
			$this->load->view('home/main_view', $data);
		}
	}	
	
	
	
	
	
}//end class

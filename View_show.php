<div class="pull-right">
	<a href="<?php echo site_url('member/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Password</th>
		<th>Email</th>
		<th>Fullname</th>
		<th>Phone</th>
		<th>Address</th>
		<th>Dob</th>
		<th>Joindate</th>
		<th>Level</th>
		<th>Flag</th>
		<th>Actions</th>
    </tr>
	<?php foreach($member as $m){ ?>
    <tr>
		<td><?php echo $m['id']; ?></td>
		<td><?php echo $m['password']; ?></td>
		<td><?php echo $m['email']; ?></td>
		<td><?php echo $m['fullname']; ?></td>
		<td><?php echo $m['phone']; ?></td>
		<td><?php echo $m['address']; ?></td>
		<td><?php echo $m['dob']; ?></td>
		<td><?php echo $m['joindate']; ?></td>
		<td><?php echo $m['level']; ?></td>
		<td><?php echo $m['flag']; ?></td>
		<td>
            <a href="<?php echo site_url('member/edit/'.$m['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('member/remove/'.$m['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>